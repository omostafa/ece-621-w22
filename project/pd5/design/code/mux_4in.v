// 4-inputs mux choices

`define MUX4_CH0	2'b00	
`define MUX4_CH1	2'b01
`define MUX4_CH2	2'b10	
`define MUX4_CH3	2'b11

module mux_4in(
  input [1:0] mux_4in_sel,
  input [31:0] mux_4in_in1,
  input [31:0] mux_4in_in2,
  input [31:0] mux_4in_in3,
  input [31:0] mux_4in_in4,
  output reg [31:0] mux_4in_out
);

always @ (*) begin
	
	if(mux_4in_sel == `MUX4_CH0)
		mux_4in_out = mux_4in_in1;
		
	else if(mux_4in_sel == `MUX4_CH1)
		mux_4in_out = mux_4in_in2;
		
	else if(mux_4in_sel == `MUX4_CH2)
		mux_4in_out = mux_4in_in3;
		
	else
		mux_4in_out = mux_4in_in4;
end
 
  
 
endmodule


