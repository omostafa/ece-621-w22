// 2-inputs mux choices

`define MUX2_CH0	1'b0	
`define MUX2_CH1	1'b1

module mux_2in(
  input mux_2in_sel,
  input [31:0] mux_2in_in1,
  input [31:0] mux_2in_in2,
  output reg [31:0] mux_2in_out
);

always @ (*) begin
	
	if(mux_2in_sel == `MUX2_CH0)
		mux_2in_out = mux_2in_in1;
	else
		mux_2in_out = mux_2in_in2;
end
 
  
 
endmodule


