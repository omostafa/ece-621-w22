
module and_gate(
  input [31:0] and_data_in1,
  input [31:0] and_data_in2,
  output reg [31:0] and_data_out
);

always @(*) begin
	and_data_out = and_data_in1 & and_data_in2;
end
 
  
 
endmodule


