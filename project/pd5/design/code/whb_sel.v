`define WORD	3'b000
`define HALF	3'b001
`define BYTE	3'b010
`define HALFU	3'b011
`define BYTEU	3'b100

module whb_sel(
  input [2:0] whb_sel,
  input [31:0] whb_sel_in,
  output reg [31:0] whb_sel_out
);


always @ (*) begin
	
	if(whb_sel == `WORD)
		whb_sel_out = whb_sel_in;
	else if (whb_sel == `HALF) begin
		if(whb_sel_in[15] == 1'b1)
			whb_sel_out = whb_sel_in | 32'hFF_FF_00_00;
		else
			whb_sel_out = whb_sel_in & 32'h00_00_FF_FF;
	end
	else if (whb_sel == `BYTE) begin
		if(whb_sel_in[7] == 1'b1)
			whb_sel_out = whb_sel_in | 32'hFF_FF_FF_00;
		else
			whb_sel_out = whb_sel_in & 32'h00_00_00_FF;
	end
	else if (whb_sel == `HALFU)
		whb_sel_out = whb_sel_in & 32'h00_00_FF_FF;
	else if (whb_sel == `BYTEU)
		whb_sel_out = whb_sel_in & 32'h00_00_00_FF;
	else
		whb_sel_out = whb_sel_in;
		
end
 
  
 
endmodule


