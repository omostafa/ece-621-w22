//`define MEM_PATH "../../verif/data/rv32ui-p-add.x"
//`define LINE_COUNT	357

// Basic definitions

`define TRUE	1'b1
`define FALSE	1'b0

// ALU operations

`define ADD 	6'b00_00_00
`define SUB 	6'b00_00_01
`define AND 	6'b00_00_10
`define OR	6'b00_00_11
`define XOR	6'b00_01_00
`define SLT	6'b00_01_01	// Less than signed
`define SLTU	6'b00_01_10	// Less than unsigned
`define SLL	6'b00_01_11	// Logic left shift
`define SRL	6'b00_10_00	// Logic right shift
`define SRA	6'b00_10_01	// Arithmetic right shift

// Immediate types

`define I_TYPE		3'b000
`define S_TYPE		3'b001
`define B_TYPE		3'b010
`define U_TYPE		3'b011
`define J_TYPE		3'b100

// R-type instructions

`define R_OPCODE 	7'b0110_011

`define ADD_ID  	10'b000_0000_000
`define SUB_ID  	10'b010_0000_000
`define SLL_ID  	10'b000_0000_001
`define SLT_ID		10'b000_0000_010
`define SLTU_ID	10'b000_0000_011
`define XOR_ID		10'b000_0000_100
`define SRL_ID		10'b000_0000_101
`define SRA_ID		10'b010_0000_101
`define OR_ID		10'b000_0000_110
`define AND_ID		10'b000_0000_111

// Immediate instructions

`define I_OPCODE	7'b0010_011

`define ADDI_ID	3'b000
`define SLTI_ID	3'b010
`define SLTIU_ID	3'b011
`define XORI_ID	3'b100
`define ORI_ID		3'b110
`define ANDI_ID	3'b111
`define SLLI_ID	3'b001
`define SRLI_SRAI_ID	3'b101

`define SRLI_UPPER	7'b000_0000
`define SRAI_UPPER	7'b010_0000

// Load instructions

`define L_OPCODE	7'b000_0011

`define LB_ID		3'b000
`define LH_ID		3'b001
`define LW_ID		3'b010
`define LBU_ID		3'b100
`define LHU_ID		3'b101

// Store instructions

`define S_OPCODE	7'b010_0011

`define SB_ID		3'b000
`define SH_ID		3'b001
`define SW_ID		3'b010

// Branch instructions

`define B_OPCODE	7'b110_0011

`define BEQ_ID		3'b000
`define BNE_ID		3'b001
`define BLT_ID		3'b100
`define BGE_ID		3'b101
`define BLTU_ID	3'b110
`define BGEU_ID	3'b111

`define B_TAKEN	2'b01
`define B_NOT_TAKEN	2'b00

`define B_MUX_CH0	2'b00	
`define B_MUX_CH1	2'b01
`define B_MUX_CH2	2'b10

// Last five instructions

`define LUI_OPCODE	7'b011_0111
`define AUIPC_OPCODE	7'b001_0111
`define JAL_OPCODE	7'b110_1111
`define JALR_OPCODE	7'b110_0111
`define ECALL_OPCODE	7'b111_0011

// Word-Half-Byte selects

`define WORD		3'b000
`define HALF		3'b001
`define BYTE		3'b010
`define HALFU		3'b011
`define BYTEU		3'b100

// Access size for dmemory

`define ONE_BYTE	2'b00
`define TWO_BYTES	2'b01
`define FOUR_BYTES	2'b10

// alu_mux choices

`define ALU_MUX_CH0	2'b00	
`define ALU_MUX_CH1	2'b01
`define ALU_MUX_CH2	2'b10
`define ALU_MUX_CH3	2'b11

// alu_imm_mux choices

`define ALU_IMM_MUX_CH0	1'b0	
`define ALU_IMM_MUX_CH1	1'b1

// dataW_mux choices

`define M_MUX_CH0	1'b0	
`define M_MUX_CH1	1'b1

// wb_mux choices

`define WB_MUX_CH0	2'b00	
`define WB_MUX_CH1	2'b01
`define WB_MUX_CH2	2'b10
`define WB_MUX_CH3	2'b11


// PD5 module

module pd5(
  input clock,
  input reset
);

  // instruction memory module

  reg [31:0] pc_f;
  wire [31:0] instr_in;
  wire [31:0] instr_out; // contains the instruction
  wire instr_wr_en;
  
  imemory imemory1( .clock(clock), .reset(reset), .instr_addr(pc_f),
		     .instr_in(instr_in), .instr_out(instr_out), 
		     .instr_wr_en(instr_wr_en) );
		    
  		    
  // data memory module
  
  reg [31:0] data_addr;
  reg [31:0] dataW;
  wire [31:0] dataR;
  reg [1:0] data_access_size;
  reg data_wr_en;
  
  dmemory dmemory1 ( .clock(clock), .reset(reset), .address(data_addr), .dataW(dataW),
  		      .dataR(dataR), .read_write(data_wr_en), .access_size(data_access_size) );
  
  // register file
  
  reg [4:0] addr_rs1;
  reg [4:0] addr_rs2;
  reg [4:0] addr_rd;
  wire [31:0] data_rs1;
  wire [31:0] data_rs2;
  reg [31:0] data_rd;
  reg reg_wr_en;
  
  register_file register_file1( .clock(clock), .write_enable(reg_wr_en),
  			 .addr_rs1(addr_rs1), .addr_rs2(addr_rs2), .addr_rd(addr_rd),
  			 .data_rs1(data_rs1), .data_rs2(data_rs2), .data_rd(data_rd));
  			 
  // ALU unit
  
  reg [5:0] alu_sel;
  reg [31:0] alu_data_in1;
  reg [31:0] alu_data_in2;
  reg [31:0] alu_data_out;
  
  alu alu1( .clock(clock), .reset(reset), .alu_sel(alu_sel), .alu_data_in1(alu_data_in1),
  	    .alu_data_in2(alu_data_in2), .alu_data_out(alu_data_out) );
  	    
  // ALU mux 1 ---> has data_rs1 as input
  
  reg [1:0] alu_mux1_sel;
  reg [31:0] alu_mux1_in1;
  reg [31:0] alu_mux1_in2;
  reg [31:0] alu_mux1_in3;
  reg [31:0] alu_mux1_in4;
  wire [31:0] alu_mux1_out;
  
  mux_4in alu_mux1( .mux_4in_sel(alu_mux1_sel), .mux_4in_in1(alu_mux1_in1),
  		     .mux_4in_in2(alu_mux1_in2), .mux_4in_in3(alu_mux1_in3),
  		     .mux_4in_in4(alu_mux1_in4), .mux_4in_out(alu_mux1_out) );
  		     
  // ALU mux 2 ---> has data_rs2 as input
  
  reg [1:0] alu_mux2_sel;
  reg [31:0] alu_mux2_in1;
  reg [31:0] alu_mux2_in2;
  reg [31:0] alu_mux2_in3;
  reg [31:0] alu_mux2_in4;
  wire [31:0] alu_mux2_out;
  
  mux_4in alu_mux2( .mux_4in_sel(alu_mux2_sel), .mux_4in_in1(alu_mux2_in1),
  		     .mux_4in_in2(alu_mux2_in2), .mux_4in_in3(alu_mux2_in3),
  		     .mux_4in_in4(alu_mux2_in4), .mux_4in_out(alu_mux2_out) );
  		     
  // ALU-PC mux
  
  reg [1:0] alu_pc_mux_sel;
  reg [31:0] alu_pc_mux_in1;
  reg [31:0] alu_pc_mux_in2;
  wire [31:0] alu_pc_mux_out;
  
  mux_4in alu_pc_mux( .mux_4in_sel(alu_pc_mux_sel), .mux_4in_in1(alu_pc_mux_in1),
  		       .mux_4in_in2(alu_pc_mux_in2), .mux_4in_in3(alu_pc_mux_in1),
  		       .mux_4in_in4(alu_pc_mux_in2), .mux_4in_out(alu_pc_mux_out) );
  
  // Immediate unit
  
  reg [2:0] igen_sel;
  reg [31:0] igen_in;
  wire [31:0] igen_out;
  
  igen igen1(.igen_sel(igen_sel), .igen_in(igen_in), .igen_out(igen_out) );
  
  // Write back mux
  
  reg [1:0] wb_mux_sel;
  reg [31:0] wb_mux_in1;
  reg [31:0] wb_mux_in2;
  reg [31:0] wb_mux_in3;
  reg [31:0] wb_mux_in4;
  wire [31:0] wb_mux_out;
  
  mux_4in wb_mux1( .mux_4in_sel(wb_mux_sel), .mux_4in_in1(wb_mux_in1), .mux_4in_in2(wb_mux_in2),
  		   .mux_4in_in3(wb_mux_in3), .mux_4in_in4(wb_mux_in4), .mux_4in_out(wb_mux_out) );
  
  // Word-Half-Byte select
  
  reg [2:0] whb_sel;
  reg [31:0] whb_sel_in;
  wire [31:0] whb_sel_out;
  
  whb_sel whb_sel1(.whb_sel(whb_sel), .whb_sel_in(whb_sel_in), .whb_sel_out(whb_sel_out) );
  
  // Branch-compare unit
  
  reg b_signed;
  reg [31:0] b_data_in1;
  reg [31:0] b_data_in2;
  wire b_eq;
  wire b_lt;
  
  b_comp b_comp1(.b_signed(b_signed), .b_data_in1(b_data_in1), .b_data_in2(b_data_in2),
  		  .b_eq(b_eq), .b_lt(b_lt) );
  		  
  // AND unit for alu_ouput <---> immediate
  
  reg [31:0] and_data_in1;
  reg [31:0] and_data_in2;
  wire [31:0] and_data_out;
  
  and_gate and_gate1(.and_data_in1(and_data_in1), .and_data_in2(and_data_in2),
  		      .and_data_out(and_data_out) );
  
  // Continuous assignments
  
  assign instr_wr_en = 0; // read-only for instructions
  
  // Pipeline registers
  
  reg [31:0] pc_d_reg_in;
  wire [31:0] pc_d_reg_out;
  
  pipeline_reg pc_d_reg(.clock(clock), .preg_in(pc_d_reg_in), .preg_out(pc_d_reg_out));
  
  reg [31:0] inst_d_reg_in;
  wire [31:0] inst_d_reg_out;
  
  pipeline_reg inst_d_reg(.clock(clock), .preg_in(inst_d_reg_in), .preg_out(inst_d_reg_out));
  
  reg [31:0] pc_x_reg_in;
  wire [31:0] pc_x_reg_out;
  
  pipeline_reg pc_x_reg(.clock(clock), .preg_in(pc_x_reg_in), .preg_out(pc_x_reg_out));
  
  reg [31:0] rs1_x_reg_in;
  wire [31:0] rs1_x_reg_out;
  
  pipeline_reg rs1_x_reg(.clock(clock), .preg_in(rs1_x_reg_in), .preg_out(rs1_x_reg_out));
  
  reg [31:0] rs2_x_reg_in;
  wire [31:0] rs2_x_reg_out;
  
  pipeline_reg rs2_x_reg(.clock(clock), .preg_in(rs2_x_reg_in), .preg_out(rs2_x_reg_out));
  
  reg [31:0] inst_x_reg_in;
  wire [31:0] inst_x_reg_out;
  
  pipeline_reg inst_x_reg(.clock(clock), .preg_in(inst_x_reg_in), .preg_out(inst_x_reg_out));
  
  reg [31:0] pc_m_reg_in;
  wire [31:0] pc_m_reg_out;
  
  pipeline_reg pc_m_reg(.clock(clock), .preg_in(pc_m_reg_in), .preg_out(pc_m_reg_out));
  
  reg [31:0] alu_m_reg_in;
  wire [31:0] alu_m_reg_out;
  
  pipeline_reg alu_m_reg(.clock(clock), .preg_in(alu_m_reg_in), .preg_out(alu_m_reg_out));
  
  reg [31:0] rs2_m_reg_in;
  wire [31:0] rs2_m_reg_out;
  
  pipeline_reg rs2_m_reg(.clock(clock), .preg_in(rs2_m_reg_in), .preg_out(rs2_m_reg_out));
  
  reg [31:0] inst_m_reg_in;
  wire [31:0] inst_m_reg_out;
  
  pipeline_reg inst_m_reg(.clock(clock), .preg_in(inst_m_reg_in), .preg_out(inst_m_reg_out));
  
  reg [31:0] imm_m_reg_in;
  wire [31:0] imm_m_reg_out;
  
  pipeline_reg imm_m_reg(.clock(clock), .preg_in(imm_m_reg_in), .preg_out(imm_m_reg_out));
  
  reg [31:0] pc_w_reg_in;
  wire [31:0] pc_w_reg_out;
  
  pipeline_reg pc_w_reg(.clock(clock), .preg_in(pc_w_reg_in), .preg_out(pc_w_reg_out));
  
  reg [31:0] data_w_reg_in;
  wire [31:0] data_w_reg_out;
  
  pipeline_reg data_w_reg(.clock(clock), .preg_in(data_w_reg_in), .preg_out(data_w_reg_out));
  
  reg [31:0] inst_w_reg_in;
  wire [31:0] inst_w_reg_out;
  
  pipeline_reg inst_w_reg(.clock(clock), .preg_in(inst_w_reg_in), .preg_out(inst_w_reg_out));
  
  reg [31:0] b_mux1_in1;
  reg [31:0] b_mux1_in2;
  reg [31:0] b_mux1_in3;
  reg [31:0] b_mux1_out;
  reg [1:0] b_mux1_sel;
  
  mux_4in b_mux1(.mux_4in_sel(b_mux1_sel), .mux_4in_in1(b_mux1_in1),
  		  .mux_4in_in2(b_mux1_in2), .mux_4in_in3(b_mux1_in3),
  		  .mux_4in_in4(b_mux1_in1), .mux_4in_out(b_mux1_out));
  
  reg [31:0] b_mux2_in1;
  reg [31:0] b_mux2_in2;
  reg [31:0] b_mux2_in3;
  reg [31:0] b_mux2_out;
  reg [1:0] b_mux2_sel;
  
  mux_4in b_mux2(.mux_4in_sel(b_mux2_sel), .mux_4in_in1(b_mux2_in1),
  		  .mux_4in_in2(b_mux2_in2), .mux_4in_in3(b_mux2_in3),
  		  .mux_4in_in4(b_mux2_in1), .mux_4in_out(b_mux2_out));
  		  
  reg [31:0] dataW_mux_in1;
  reg [31:0] dataW_mux_in2;
  reg [31:0] dataW_mux_out;
  reg dataW_mux_sel;
  
  mux_2in dataW_mux(.mux_2in_sel(dataW_mux_sel), .mux_2in_in1(dataW_mux_in1),
  		      .mux_2in_in2(dataW_mux_in2), .mux_2in_out(dataW_mux_out));
  
  reg [31:0] alu_imm_mux_in1;
  reg [31:0] alu_imm_mux_in2;
  reg [31:0] alu_imm_mux_out;
  reg alu_imm_mux_sel;
  
  mux_2in alu_imm_mux(.mux_2in_sel(alu_imm_mux_sel), .mux_2in_in1(alu_imm_mux_in1),
  		       .mux_2in_in2(alu_imm_mux_in2), .mux_2in_out(alu_imm_mux_out));
  		       
  

  
  // Incrementing PC or branching
  
  
    always @ (posedge clock) begin // This is ROM so no need to write to it
  	if(!reset) begin
  		pc_f <= alu_pc_mux_out;
  	end
  	else begin
  		pc_f <= 32'h01000000;
  	end
    end
  
  
  always @ (*) begin
    
	pc_d_reg_in = pc_f;
	inst_d_reg_in = instr_out;
	
	addr_rs1 = inst_d_reg_out[19:15];
	addr_rs2 = inst_d_reg_out[24:20];
	
	pc_x_reg_in = pc_d_reg_out;
	rs1_x_reg_in = data_rs1;
	rs2_x_reg_in = data_rs2;
	inst_x_reg_in = inst_d_reg_out;
	
	b_mux1_in1 = rs1_x_reg_out;
	b_mux1_in2 = alu_m_reg_out;
	b_mux1_in3 = data_w_reg_out;
	
	b_mux2_in1 = rs2_x_reg_out;
	b_mux2_in2 = alu_m_reg_out;
	b_mux2_in3 = data_w_reg_out;
	
	b_data_in1 = b_mux1_out;
	b_data_in2 = b_mux2_out;
	igen_in = inst_x_reg_out;
	
	alu_mux1_in1 = rs1_x_reg_out;
	alu_mux1_in2 = pc_x_reg_out;
	alu_mux1_in3 = alu_m_reg_out;
	alu_mux1_in4 = data_w_reg_out;
	
	alu_mux2_in1 = rs2_x_reg_out;
	alu_mux2_in2 = igen_out;
	alu_mux2_in3 = alu_m_reg_out;
	alu_mux2_in4 = data_w_reg_out;
	
	alu_data_in1 = alu_mux1_out;
	alu_data_in2 = alu_mux2_out;
	
	pc_m_reg_in = pc_x_reg_out;
	and_data_in1 = alu_data_out;
	alu_pc_mux_in1 = pc_f + 4;
	alu_pc_mux_in2 = and_data_out;
	
	imm_m_reg_in = igen_out;
	alu_imm_mux_in1 = alu_data_out;
	alu_imm_mux_in2 = igen_out;
	
	alu_m_reg_in = alu_imm_mux_out;
	rs2_m_reg_in = rs2_x_reg_out;
	inst_m_reg_in = inst_x_reg_out;
	
	dataW_mux_in1 = rs2_m_reg_out;
	dataW_mux_in2 = data_w_reg_out;
	
	data_addr = alu_m_reg_out;
	dataW = dataW_mux_out;
	
	whb_sel_in = dataR;
	wb_mux_in1 = whb_sel_out;
	wb_mux_in2 = alu_m_reg_out;
	wb_mux_in3 = pc_m_reg_out + 4;
	wb_mux_in4 = imm_m_reg_out;
	
	data_w_reg_in = wb_mux_out;
	inst_w_reg_in = inst_m_reg_out;
	pc_w_reg_in = pc_m_reg_out;
	
	data_rd = data_w_reg_out;
	addr_rd = inst_w_reg_out[11:7];

  	
  end
  
  // ------------------------------------
  
  // General regs
  
  reg b_taken;
  
  // Regs for pipelining instructions
  
  	reg [6:0] opcode_d;
	reg [2:0] funct3_d;
	reg [6:0] funct7_d;
	reg [4:0] dec_rs1;
	reg [4:0] dec_rs2;
	reg [4:0] dec_rd;
  
	reg [6:0] opcode_x;
	reg [2:0] funct3_x;
	reg [6:0] funct7_x;
	reg [2:0] B_ID_x;
	reg [9:0] R_ID_x;
	reg [2:0] I_ID_x;
	reg [2:0] L_ID_x;
	reg [2:0] S_ID_x;
	reg [4:0] ex_rs1;
	reg [4:0] ex_rs2;
	reg [4:0] ex_rd;

	reg [6:0] opcode_m;
	reg [2:0] L_ID_m;
	reg [2:0] S_ID_m;
	reg [4:0] mem_rs1;
	reg [4:0] mem_rs2;
	reg [4:0] mem_rd;

	reg [6:0] opcode_w;
	reg [4:0] wb_rs1;
	reg [4:0] wb_rs2;
	reg [4:0] wb_rd;
	
	always @ (*) begin
	
		opcode_d = inst_d_reg_out[6:0];
		funct3_d = inst_d_reg_out[14:12];
		funct7_d = inst_d_reg_out[31:25];
		dec_rs1 = inst_d_reg_out[19:15];
		dec_rs2 = inst_d_reg_out[24:20];
		dec_rd = inst_d_reg_out[11:7];
		
		opcode_x = inst_x_reg_out[6:0];
		funct3_x = inst_x_reg_out[14:12];
		funct7_x = inst_x_reg_out[31:25];
		B_ID_x = inst_x_reg_out[14:12];
		R_ID_x = {funct7_x, funct3_x};
		I_ID_x = inst_x_reg_out[14:12];
		L_ID_x = inst_x_reg_out[14:12];
		S_ID_x = inst_x_reg_out[14:12];
		ex_rs1 =  inst_x_reg_out[19:15];
		ex_rs2 =  inst_x_reg_out[24:20];
		ex_rd = inst_x_reg_out[11:7];
		
		opcode_m = inst_m_reg_out[6:0];
		L_ID_m = inst_m_reg_out[14:12];
		S_ID_m = inst_m_reg_out[14:12];
		mem_rs1 =  inst_m_reg_out[19:15];
		mem_rs2 =  inst_m_reg_out[24:20];
		mem_rd  =  inst_m_reg_out[11:7];
		
		opcode_w = inst_w_reg_out[6:0];
		wb_rs1 =  inst_w_reg_out[19:15];
		wb_rs2 =  inst_w_reg_out[24:20];
		wb_rd  =  inst_w_reg_out[11:7];
	
	end
  
  // -------------------------------------
  
  // Regs for pipelining control
  
  reg mem_writes_rd;
  reg wb_writes_rd;
  
  // -------------------------------------
  
  // Control signals for the execute stage
  
  
  always @ (*) begin
	
	case (opcode_x)
  		`R_OPCODE: begin
  		
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_taken = `FALSE;
  			b_signed = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			if(mem_writes_rd && (mem_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH2; // MX bypass
  			else if(wb_writes_rd && (wb_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH3; // WX bypass
  			else
				alu_mux1_sel = `ALU_MUX_CH0; // normal operation
			
			if(mem_writes_rd && (mem_rd == ex_rs2))
				alu_mux2_sel = `ALU_MUX_CH2; // MX bypass
			else if(wb_writes_rd && (wb_rd == ex_rs2))
				alu_mux2_sel = `ALU_MUX_CH3; // WX bypass
			else
				alu_mux2_sel = `ALU_MUX_CH0; // normal operation
				
			igen_sel = `I_TYPE;
			and_data_in2 = 32'hFF_FF_FF_FF;
			alu_pc_mux_sel = `B_NOT_TAKEN;
			
  			if(R_ID_x == `ADD_ID) begin
  				alu_sel = `ADD;
  			end
  			else if(R_ID_x == `SUB_ID) begin
  				alu_sel = `SUB;
  			end
  			else if(R_ID_x == `SLL_ID) begin
  				alu_sel = `SLL;
  			end
  			else if(R_ID_x == `SLT_ID) begin
  				alu_sel = `SLT;
  			end
  			else if(R_ID_x == `SLTU_ID) begin
  				alu_sel = `SLTU;
  			end
  			else if(R_ID_x == `XOR_ID) begin
  				alu_sel = `XOR;
  			end
  			else if(R_ID_x == `SRL_ID) begin
  				alu_sel = `SRL;
  			end
  			else if(R_ID_x == `SRA_ID) begin
  				alu_sel = `SRA;
  			end
  			else if(R_ID_x == `OR_ID) begin
  				alu_sel = `OR;
  			end
  			else if(R_ID_x == `AND_ID) begin
  				alu_sel = `AND;
  			end
  			else begin // redundant
				alu_sel = `ADD;
			end
  		end
  		`I_OPCODE: begin
  		
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_taken = `FALSE;
  			b_signed = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			if(mem_writes_rd && (mem_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH2; // MX bypass
  			else if(wb_writes_rd && (wb_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH3; // WX bypass
  			else
  				alu_mux1_sel = `ALU_MUX_CH0;
  			
  			alu_mux2_sel = `ALU_MUX_CH1;
				
			igen_sel = `I_TYPE;
  			and_data_in2 = 32'hFF_FF_FF_FF;
  			alu_pc_mux_sel = `B_NOT_TAKEN;
  			
  			if(I_ID_x == `ADDI_ID) begin
  				alu_sel = `ADD;
  			end
  			else if(I_ID_x == `SLTI_ID) begin
  				alu_sel = `SLT;
  			end
  			else if(I_ID_x == `SLTIU_ID) begin
  				alu_sel = `SLTU;
  			end
  			else if(I_ID_x == `XORI_ID) begin
  				alu_sel = `XOR;
  			end
  			else if(I_ID_x == `ORI_ID) begin
  				alu_sel = `OR;
  			end
  			else if(I_ID_x == `ANDI_ID) begin
  				alu_sel = `AND;
  			end
  			else if(I_ID_x == `SLLI_ID) begin
  				alu_sel = `SLL;
  			end
  			else if(I_ID_x == `SRLI_SRAI_ID) begin
  				if(instr_out[31:25] == `SRLI_UPPER) begin
  					alu_sel = `SRL;
  				end
  				else if(instr_out[31:25] == `SRAI_UPPER) begin
  					alu_sel = `SRA;
  				end
  				else begin // redundant
  					alu_sel = `ADD;
  				end
  			end
  			else begin // redundant
				alu_sel = `ADD;
			end
  		end
  		`L_OPCODE: begin  // Loads use whb_sel
  		
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_taken = `FALSE;
  			b_signed = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			if(mem_writes_rd && (mem_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH2; // MX bypass
  			else if(wb_writes_rd && (wb_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH3; // WX bypass
  			else
  				alu_mux1_sel = `ALU_MUX_CH0;
  			
  			alu_mux2_sel = `ALU_MUX_CH1;
			
			igen_sel = `I_TYPE;
			and_data_in2 = 32'hFF_FF_FF_FF;
			alu_pc_mux_sel = `B_NOT_TAKEN;;
			alu_sel = `ADD;
			
  		end
  		`S_OPCODE: begin  // Stores use access size
  		
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_taken = `FALSE;
  			b_signed = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			if(mem_writes_rd && (mem_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH2; // MX bypass
  			else if(wb_writes_rd && (wb_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH3; // WX bypass
  			else
  				alu_mux1_sel = `ALU_MUX_CH0;
  			
  			alu_mux2_sel = `ALU_MUX_CH1;
  			
			igen_sel = `S_TYPE;
			and_data_in2 = 32'hFF_FF_FF_FF;
			alu_pc_mux_sel = `B_NOT_TAKEN;
			alu_sel = `ADD;
			
  		end
  		`B_OPCODE: begin
  		
			igen_sel = `B_TYPE;
			and_data_in2 = 32'hFF_FF_FF_FF;
			alu_sel = `ADD;
			
			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
			
			if(mem_writes_rd && (mem_rd == ex_rs1))
				b_mux1_sel = `B_MUX_CH1; // MX bypassing
			else if(wb_writes_rd && (wb_rd == ex_rs1))
				b_mux1_sel = `B_MUX_CH2; // WX bypassing
			else
				b_mux1_sel = `B_MUX_CH0;
			
			if(mem_writes_rd && (mem_rd == ex_rs2))
				b_mux2_sel = `B_MUX_CH1; // MX bypassing
			else if(wb_writes_rd && (wb_rd == ex_rs1))
				b_mux2_sel = `B_MUX_CH2; // WX bypassing
			else
				b_mux2_sel = `B_MUX_CH0;
				
			alu_mux1_sel = `ALU_MUX_CH1;
  			alu_mux2_sel = `ALU_MUX_CH1;
  		
  			if(B_ID_x == `BEQ_ID) begin
  				b_signed = `FALSE;
  				if(b_eq) begin
  					b_taken = `TRUE;
  					alu_pc_mux_sel = `B_TAKEN;
  				end
  				else begin
  					b_taken = `FALSE;
  					alu_pc_mux_sel = `B_NOT_TAKEN;
  				end
  			end
  			else if(B_ID_x == `BNE_ID) begin
  				b_signed = `FALSE;
  				if(!b_eq) begin
  					b_taken = `TRUE;
  					alu_pc_mux_sel = `B_TAKEN;
  				end
  				else begin
  					b_taken = `FALSE;
  					alu_pc_mux_sel = `B_NOT_TAKEN;
  				end
  			end
  			else if(B_ID_x == `BLT_ID) begin
  				b_signed = `TRUE;
  				if(b_lt) begin
  					b_taken = `TRUE;
  					alu_pc_mux_sel = `B_TAKEN;
  				end
  				else begin
  					b_taken = `FALSE;
  					alu_pc_mux_sel = `B_NOT_TAKEN;
  				end
  			end
  			else if(B_ID_x == `BGE_ID) begin
  				b_signed = `TRUE;
  				if(!b_lt) begin
  					b_taken = `TRUE;
  					alu_pc_mux_sel = `B_TAKEN;
  				end
  				else begin
  					b_taken = `FALSE;
  					alu_pc_mux_sel = `B_NOT_TAKEN;
  				end
  			end
  			else if(B_ID_x == `BLTU_ID) begin
  				b_signed = `FALSE;
  				if(b_lt) begin
  					b_taken = `TRUE;
  					alu_pc_mux_sel = `B_TAKEN;
  				end
  				else begin
  					b_taken = `FALSE;
  					alu_pc_mux_sel = `B_NOT_TAKEN;
  				end
  			end
  			else if(B_ID_x == `BGEU_ID) begin
  				b_signed = `FALSE;
  				if(!b_lt) begin
  					b_taken = `TRUE;
  					alu_pc_mux_sel = `B_TAKEN;
  				end
  				else begin
  					b_taken = `FALSE;
  					alu_pc_mux_sel = `B_NOT_TAKEN;
  				end
  			end
  			else begin // redundant
  				b_signed = `FALSE;
  				b_taken = `FALSE;
	  			alu_pc_mux_sel = `B_NOT_TAKEN;
	  		end
  		end
  		`JALR_OPCODE: begin
  		
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_signed = `FALSE;
  			b_taken = `TRUE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			igen_sel = `I_TYPE;
  			alu_sel = `ADD;
  			
  			if(mem_writes_rd && (mem_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH2; // MX bypass
  			else if(wb_writes_rd && (wb_rd == ex_rs1))
  				alu_mux1_sel = `ALU_MUX_CH3; // WX bypass
  			else
  				alu_mux1_sel = `ALU_MUX_CH0;
  				
  			alu_mux2_sel = `ALU_MUX_CH1;
  				
  			alu_pc_mux_sel = `B_TAKEN;
  			and_data_in2 = 32'hFF_FF_FF_FE;
  		end
  		`JAL_OPCODE: begin // Jump relative to PC
  		
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_signed = `FALSE;
  			b_taken = `TRUE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			igen_sel = `J_TYPE;
  			alu_sel = `ADD;
  			
  			alu_mux1_sel = `ALU_MUX_CH1;
  			alu_mux2_sel = `ALU_MUX_CH1;
  			
  			alu_pc_mux_sel = `B_TAKEN;
  			and_data_in2 = 32'hFF_FF_FF_FE;
  		end
  		`AUIPC_OPCODE: begin
  			
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_signed = `FALSE;
  			b_taken = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			igen_sel = `U_TYPE;
  			alu_sel = `ADD;
  			
  			alu_mux1_sel = `ALU_MUX_CH1;
  			alu_mux2_sel = `ALU_MUX_CH1;
  				
  			alu_pc_mux_sel = `B_NOT_TAKEN; // normal pc operation
  			and_data_in2 = 32'hFF_FF_FF_FF;
  		end
  		`LUI_OPCODE: begin // Load U-type immediate to register
  			
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_signed = `FALSE;
  			b_taken = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH1;
  			
  			igen_sel = `U_TYPE;
  			alu_sel = `ADD; //redundant
  			
  			alu_mux1_sel = `ALU_MUX_CH1;
  			alu_mux2_sel = `ALU_MUX_CH1;
  			
  			alu_pc_mux_sel = `B_NOT_TAKEN;
  			and_data_in2 = 32'hFF_FF_FF_FF;
  		end
  		`ECALL_OPCODE: begin
  			
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_signed = `FALSE;
  			b_taken = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  			
  			igen_sel = `I_TYPE;
			alu_sel = `ADD;
			alu_mux1_sel = `ALU_MUX_CH0;
			alu_mux2_sel = `ALU_MUX_CH0;
			alu_pc_mux_sel = `B_NOT_TAKEN;
			and_data_in2 = 32'hFF_FF_FF_FF;
  		end
  		default: begin
  		
  			b_mux1_sel = `B_MUX_CH0;
  			b_mux2_sel = `B_MUX_CH0;
  			b_signed = `FALSE;
  			b_taken = `FALSE;
  			
  			alu_imm_mux_sel = `ALU_IMM_MUX_CH0;
  				
  			igen_sel = `I_TYPE;
			alu_sel = `ADD;
			alu_mux1_sel = `ALU_MUX_CH0;
			alu_mux2_sel = `ALU_MUX_CH0;
			alu_pc_mux_sel = `B_NOT_TAKEN;
			and_data_in2 = 32'hFF_FF_FF_FF;
  		end
  	endcase
  
  end
  
  //--------------------------------------
  	
  // Control signals for the memory stage
  
  always @ (*) begin
	
  	case (opcode_m)
  		
  		`R_OPCODE: begin
  			data_access_size = `FOUR_BYTES;
			whb_sel = `WORD;
			wb_mux_sel = `WB_MUX_CH1;
			data_wr_en = `FALSE; // No data written to dmemory
  			mem_writes_rd = `TRUE;
  			dataW_mux_sel = `M_MUX_CH0;
  		end
  		
  		`I_OPCODE: begin
  			data_access_size = `FOUR_BYTES;
			whb_sel = `WORD;
			wb_mux_sel = `WB_MUX_CH1;
			data_wr_en = `FALSE; // No data written to dmemory
  			mem_writes_rd = `TRUE;
  			dataW_mux_sel = `M_MUX_CH0;
  		end
  		
  		`L_OPCODE: begin  // Loads use whb_sel
  		
  			mem_writes_rd = `TRUE;
  			dataW_mux_sel = `M_MUX_CH0;
  			data_wr_en = `FALSE; // No data written to dmemory
  			wb_mux_sel = `WB_MUX_CH0;
  			data_access_size = `FOUR_BYTES; // don't care
  			
  			if(L_ID_m == `LW_ID) begin
  				whb_sel = `WORD;
  			end
  			else if(L_ID_m == `LH_ID) begin
  				whb_sel = `HALF;
  			end
  			else if(L_ID_m == `LB_ID) begin
  				whb_sel = `BYTE;
  			end
  			else if(L_ID_m == `LHU_ID) begin
  				whb_sel = `HALFU;
  			end
  			else if(L_ID_m == `LBU_ID) begin
  				whb_sel = `BYTEU;
  			end
  			else begin // redundant
				whb_sel = `WORD;
			end
  		end
  		
  		`S_OPCODE: begin  // Stores use access size
  		
  			mem_writes_rd = `FALSE;
			whb_sel = `WORD; // don't care
			wb_mux_sel = `WB_MUX_CH0; // don't care
			data_wr_en = `TRUE; // Data IS written to dmemory
  			
  			if(wb_writes_rd && (wb_rd == mem_rs2))
  				dataW_mux_sel = `M_MUX_CH1; // WM bypas
  			else
  				dataW_mux_sel = `M_MUX_CH0;
  			
  			if(S_ID_m == `SB_ID) begin
  				data_access_size = `ONE_BYTE;
  			end
  			else if(S_ID_m == `SH_ID) begin
  				data_access_size = `TWO_BYTES;
  			end
  			else if(S_ID_m == `SW_ID) begin
  				data_access_size = `FOUR_BYTES;
  			end
  			else begin // redundant
  				data_access_size = `FOUR_BYTES;
			end
  		end
  		
  		`B_OPCODE: begin
  			mem_writes_rd = `FALSE;
  			data_access_size = `FOUR_BYTES;
  			whb_sel = `WORD;
  			wb_mux_sel = `WB_MUX_CH0; // don't care
  			data_wr_en = `FALSE;
  			dataW_mux_sel = `M_MUX_CH0;
  		end
  		
  		`JALR_OPCODE: begin
  			mem_writes_rd = `TRUE;
  			data_access_size = `FOUR_BYTES;
  			whb_sel = `WORD;
  			wb_mux_sel = `WB_MUX_CH2; // Write PC+4 to rd
  			data_wr_en = `FALSE;
  			dataW_mux_sel = `M_MUX_CH0;
  		end
  		`JAL_OPCODE: begin // Jump relative to PC
  			mem_writes_rd = `TRUE;
  			data_access_size = `FOUR_BYTES;
  			whb_sel = `WORD;
  			wb_mux_sel = `WB_MUX_CH2; // Write PC+4 to rd
  			data_wr_en = `FALSE;
  			dataW_mux_sel = `M_MUX_CH0;
  		end
  		`AUIPC_OPCODE: begin
  			mem_writes_rd = `TRUE;
  			data_access_size = `FOUR_BYTES;
  			whb_sel = `WORD;
  			wb_mux_sel = `WB_MUX_CH1; // Write alu-ouput to rd
  			data_wr_en = `FALSE;
  			dataW_mux_sel = `M_MUX_CH0;
  		end
  		`LUI_OPCODE: begin // Load U-type immediate to register
  			mem_writes_rd = `TRUE;
  			data_access_size = `FOUR_BYTES;
  			whb_sel = `WORD;
  			wb_mux_sel = `WB_MUX_CH3;
  			data_wr_en = `FALSE;
  			dataW_mux_sel = `M_MUX_CH0;
  		end
  		`ECALL_OPCODE: begin
  			mem_writes_rd = `FALSE;
  			data_access_size = `FOUR_BYTES;
  			whb_sel = `WORD;
			wb_mux_sel = `WB_MUX_CH0;
			data_wr_en = `FALSE;
			dataW_mux_sel = `M_MUX_CH0;
  		end
  		default: begin
  			mem_writes_rd = `FALSE;
  			data_access_size = `FOUR_BYTES;
  			whb_sel = `WORD;
			wb_mux_sel = `WB_MUX_CH0;
			data_wr_en = `FALSE;
			dataW_mux_sel = `M_MUX_CH0;
  		end
  	endcase
  end
  	
  //--------------------------------------
  
  // Control signals for write back stage

  
  always @ (*) begin
  
  	case (opcode_w)
  		`R_OPCODE: begin
  			reg_wr_en = `TRUE;
  			wb_writes_rd = `TRUE;
  		end
  		`I_OPCODE: begin
  			reg_wr_en = `TRUE;
  			wb_writes_rd = `TRUE;
  		end
  		`L_OPCODE: begin  // Loads use whb_sel
  			reg_wr_en = `TRUE;
  			wb_writes_rd = `TRUE;
  		end
  		`S_OPCODE: begin  // Stores use access size
  			reg_wr_en = `FALSE;
  			wb_writes_rd = `FALSE;
  		end
  		`B_OPCODE: begin
  			reg_wr_en = `FALSE;
  			wb_writes_rd = `FALSE;
  		end
  		`JALR_OPCODE: begin
  			reg_wr_en = `TRUE;
  			wb_writes_rd = `TRUE;
  		end
  		`JAL_OPCODE: begin // Jump relative to PC
  			reg_wr_en = `TRUE;
  			wb_writes_rd = `TRUE;
  		end
  		`AUIPC_OPCODE: begin
  			reg_wr_en = `TRUE;
  			wb_writes_rd = `TRUE;
  		end
  		`LUI_OPCODE: begin // Load U-type immediate to register
  			reg_wr_en = `TRUE;
  			wb_writes_rd = `TRUE;
  		end
  		`ECALL_OPCODE: begin
			reg_wr_en = `FALSE;
			wb_writes_rd = `FALSE;
  		end
  		default: begin
			reg_wr_en = `FALSE;
			wb_writes_rd = `FALSE;
  		end
  	endcase
  end
 
  		    
  //--------------------------------------




endmodule
