
module pipeline_reg(
  input 	clock,
  input	[31:0] preg_in,
  output reg	[31:0] preg_out
);


always @ (posedge clock) begin

	preg_out <= preg_in;
		
end
 
  
 
endmodule


