
/* Your Code Below! Enable the following define's 
 * and replace ??? with actual wires */
// ----- signals -----
// You will also need to define PC properly
`define F_PC                pc_f
`define F_INSN              instr_out

`define D_PC                pc_d_reg_out
`define D_OPCODE            opcode_d
`define D_RD                dec_rd
`define D_RS1               dec_rs1
`define D_RS2               dec_rs2
`define D_FUNCT3            funct3_d
`define D_FUNCT7            funct7_d
`define D_IMM               igen_out
`define D_SHAMT             dec_rs2

`define R_WRITE_ENABLE      reg_wr_en
`define R_WRITE_DESTINATION addr_rd
`define R_WRITE_DATA        data_rd
`define R_READ_RS1          addr_rs1
`define R_READ_RS2          addr_rs2
`define R_READ_RS1_DATA     data_rs1
`define R_READ_RS2_DATA     data_rs2

`define E_PC                pc_x_reg_out
`define E_ALU_RES           alu_data_out
`define E_BR_TAKEN          b_taken

`define M_PC                pc_m_reg_out
`define M_ADDRESS           alu_m_reg_out
`define M_RW                data_wr_en
`define M_SIZE_ENCODED      data_access_size
`define M_DATA              dataR

`define W_PC                pc_w_reg_out
`define W_ENABLE            reg_wr_en
`define W_DESTINATION       addr_rd
`define W_DATA              data_w_reg_out

// ----- signals -----

// ----- design -----
`define TOP_MODULE                 pd5
// ----- design -----
