module registers(
  input clock,
  input reset,
  input wr_en,
  input reg [4:0] addrA,
  input reg [4:0] addrB,
  input reg [4:0] addrD, // address to write to
  output reg [31:0] dataA, // corresponds to rs1
  output reg [31:0] dataB, // corresponds to rs2
  input reg [31:0] dataD // data to write into a register
);

reg [31:0] registers [0:31];

always @ (addrA) begin
	dataA = registers[addrA];
end

always @ (addrB) begin
	dataB = registers[addrB];
end

always @ (posedge clock) begin
	if(wr_en)
		registers[addrD] = dataD;
end
 
  
 
endmodule


