
module and_gate(
  input reg [31:0] and_data_in1,
  input reg [31:0] and_data_in2,
  output reg [31:0] and_data_out
);

assign and_data_out = and_data_in1 & and_data_in2;
 
  
 
endmodule


