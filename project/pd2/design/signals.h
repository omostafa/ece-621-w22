
/* Your Code Below! Enable the following define's 
 * and replace ??? with actual wires */
// ----- signals -----
// You will also need to define PC properly
`define F_PC                instr_addr
`define F_INSN              instr_out

`define D_PC                instr_addr
`define D_OPCODE            opcode
`define D_RD                addrD
`define D_RS1               addrA
`define D_RS2               addrB
`define D_FUNCT3            funct3
`define D_FUNCT7            funct7
`define D_IMM               igen_out
`define D_SHAMT             dataB


// ----- signals -----

// ----- design -----
`define TOP_MODULE                 pd2
// ----- design -----
