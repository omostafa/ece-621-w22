module reg_and_arst(
  input  wire clock,
  input  wire areset,
  input  wire x,
  input  wire y,
  output reg out
);

reg z; 

assign_and assign_and_arst(
	.x(x), .y(y), .z(z)
);

  always @(posedge clock or posedge areset) begin
    if (areset)
	out <= 0;
    else
	out <= z;
  end
	
endmodule
