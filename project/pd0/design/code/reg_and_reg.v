module reg_and_reg(
  input wire clock,
  input wire reset,
  input wire x,
  input wire y,
  output reg out
);

reg x_out;
reg y_out;
reg z; 
  

	assign_and assign_and_reg(
		.x(x_out), .y(y_out), .z(z)
	);

always @ (posedge clock) begin
	if(reset)
		x_out <= 0;
	else
		x_out <= x;
end

always @ (posedge clock) begin
	if(reset)
		y_out <= 0;
	else
		y_out <= y;
end

always @ (posedge clock) begin
	if(reset)
		out <= 0;
	else
		out <= z;
end
		

endmodule
