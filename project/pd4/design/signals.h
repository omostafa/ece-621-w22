
/* Your Code Below! Enable the following define's 
 * and replace ??? with actual wires */
// ----- signals -----
// You will also need to define PC properly
`define F_PC                instr_addr
`define F_INSN              instr_out

`define D_PC                instr_addr
`define D_OPCODE            opcode
`define D_RD                addr_rd
`define D_RS1               addr_rs1
`define D_RS2               addr_rs2
`define D_FUNCT3            funct3
`define D_FUNCT7            funct7
`define D_IMM               igen_out
`define D_SHAMT             data_rs2

`define R_WRITE_ENABLE      write_enable
`define R_WRITE_DESTINATION addr_rd
`define R_WRITE_DATA        data_rd
`define R_READ_RS1          addr_rs1
`define R_READ_RS2          addr_rs2
`define R_READ_RS1_DATA     data_rs1
`define R_READ_RS2_DATA     data_rs2

`define E_PC                instr_addr
`define E_ALU_RES           alu_data_out
`define E_BR_TAKEN          b_taken

`define M_PC                instr_addr
`define M_ADDRESS           data_addr
`define M_RW                data_wr_en
`define M_SIZE_ENCODED      data_access_size
`define M_DATA              dataW

`define W_PC                instr_addr
`define W_ENABLE            reg_wr_en
`define W_DESTINATION       addr_rd
`define W_DATA              wb_mux_out

// ----- signals -----

// ----- design -----
`define TOP_MODULE                 pd4
// ----- design -----
