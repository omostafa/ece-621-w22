`define I_TYPE		3'b000
`define S_TYPE		3'b001
`define B_TYPE		3'b010
`define U_TYPE		3'b011
`define J_TYPE		3'b100

module igen(
  input [2:0] igen_sel,
  input [31:0] igen_in,
  output reg [31:0] igen_out
);



always @ (*) begin
	
	if(igen_sel == `I_TYPE)
		igen_out = { {21{igen_in[31]}}, igen_in[30:25], igen_in[24:20] };
		
	else if(igen_sel == `S_TYPE)
		igen_out = { {21{igen_in[31]}}, igen_in[30:25], igen_in[11:7] };
		
	else if(igen_sel == `B_TYPE)
		igen_out = { {20{igen_in[31]}}, igen_in[7], igen_in[30:25], igen_in[11:8], 1'b0 };
		
	else if(igen_sel == `J_TYPE)
		igen_out = { {12{igen_in[31]}}, igen_in[19:12], igen_in[20], igen_in[30:21], 1'b0 };
	
	else if(igen_sel == `U_TYPE)
		igen_out = {igen_in[31:12], 12'b0000_0000_0000};
		
	else
		igen_out = 32'b0;
end
 
  
 
endmodule


