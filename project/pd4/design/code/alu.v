`define ADD 	6'b00_00_00
`define SUB 	6'b00_00_01
`define AND 	6'b00_00_10
`define OR	6'b00_00_11
`define XOR	6'b00_01_00
`define SLT	6'b00_01_01	// Less than signed
`define SLTU	6'b00_01_10	// Less than unsigned
`define SLL	6'b00_01_11	// Logic left shift
`define SRL	6'b00_10_00	// Logic right shift
`define SRA	6'b00_10_01	// Arithmetic right shift

module alu(
  input clock,
  input reset,
  input [5:0] alu_sel,
  input [31:0]  alu_data_in1,
  input [31:0]  alu_data_in2,
  output reg [31:0] alu_data_out
);


always @ (*) begin
	if(alu_sel == `ADD) begin
		if (alu_data_in1[31] == 1'b0 && alu_data_in2[31] == 1'b0)
			alu_data_out = alu_data_in1 + alu_data_in2;
			
		else if (alu_data_in1[31] == 1'b0 && alu_data_in2[31] == 1'b1)
			alu_data_out = alu_data_in1 - ( (~alu_data_in2) + 1);
			
		else if (alu_data_in1[31] == 1'b1 && alu_data_in2[31] == 1'b0)
			alu_data_out = alu_data_in2 - ( (~alu_data_in1) + 1);
			
		else	
			alu_data_out = ~( ( (~alu_data_in1) + 1) + ( (~alu_data_in2) + 1)) + 1;
	end
	else if(alu_sel == `SUB) begin
		if (alu_data_in1[31] == 1'b0 && alu_data_in2[31] == 1'b0)
			alu_data_out = alu_data_in1 - alu_data_in2;
			
		else if (alu_data_in1[31] == 1'b0 && alu_data_in2[31] == 1'b1)
			alu_data_out = alu_data_in1 + ( (~alu_data_in2) + 1);
			
		else if (alu_data_in1[31] == 1'b1 && alu_data_in2[31] == 1'b0)
			alu_data_out = ~( ( (~alu_data_in1) + 1) + alu_data_in2 ) + 1;
			
		else	
			alu_data_out = ((~alu_data_in2) + 1) - ( (~alu_data_in1) + 1);
	end
	else if(alu_sel == `AND) begin
		alu_data_out = alu_data_in1 & alu_data_in2;
	end
	else if(alu_sel == `OR) begin
		alu_data_out = alu_data_in1 | alu_data_in2;
	end
	else if(alu_sel == `XOR) begin
		alu_data_out = alu_data_in1 ^ alu_data_in2;
	end
	else if(alu_sel == `SLT) begin
		if (alu_data_in1[31] == 1'b0 && alu_data_in2[31] == 1'b0)
			alu_data_out = {31'b0, alu_data_in1 < alu_data_in2};
		else if (alu_data_in1[31] == 1'b1 && alu_data_in2[31] == 1'b0)
			alu_data_out = 32'b1;
		else if (alu_data_in1[31] == 1'b0 && alu_data_in2[31] == 1'b1)
			alu_data_out = 32'b0;
		else begin	
			alu_data_out = {31'b0, ((~alu_data_in1) + 1'b1) > ((~alu_data_in2)+1'b1) };	 
		end

	end
	else if(alu_sel == `SLTU) begin
		alu_data_out = {31'b0, alu_data_in1 < alu_data_in2};
	end
	else if(alu_sel == `SLL) begin // Only lower 5 bits are used for the shift
		alu_data_out = alu_data_in1 << {27'h0, alu_data_in2[4:0]};
	end
	else if(alu_sel == `SRL) begin
		alu_data_out = alu_data_in1 >> {27'h0, alu_data_in2[4:0]};
	end
	else if(alu_sel == `SRA) begin
		alu_data_out = $signed(alu_data_in1) >>> {27'h0, alu_data_in2[4:0]};
	end
	else
		alu_data_out = 32'h0;
end

endmodule
