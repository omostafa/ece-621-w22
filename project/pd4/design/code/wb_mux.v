module wb_mux(
  input [1:0] wb_mux_sel,
  input [31:0] wb_mux_in1,
  input [31:0] wb_mux_in2,
  input [31:0] wb_mux_in3,
  input [31:0] wb_mux_in4,
  output reg [31:0] wb_mux_out
);

always @ (*) begin
	
	if(wb_mux_sel == 2'b00)
		wb_mux_out = wb_mux_in1;
		
	else if(wb_mux_sel == 2'b01)
		wb_mux_out = wb_mux_in2;
		
	else if(wb_mux_sel == 2'b10)
		wb_mux_out = wb_mux_in3;
	else
		wb_mux_out = wb_mux_in4;
end
 
  
 
endmodule


