//`define MEM_DEPTH	32'd1048576

`define ONE_BYTE		2'b00
`define TWO_BYTES		2'b01
`define FOUR_BYTES		2'b10


module dmemory(
  input clock,
  input reset,
  input [31:0] address,
  input [31:0] dataW,
  input read_write,
  input [1:0] access_size,
  output reg [31:0] dataR
);

  reg [7:0] data_memory [0:`MEM_DEPTH];
  
always @ (*) begin
	if(!read_write)
		dataR = {data_memory[address+3], data_memory[address+2],
			 data_memory[address+1], data_memory[address]};
	else
		dataR = 32'h0;
end 
 
always @ (posedge clock) begin
	if (read_write) begin
		if (access_size == `FOUR_BYTES) begin
			data_memory[address] = dataW[7:0];
			data_memory[address+1] = dataW[15:8];
			data_memory[address+2] = dataW[23:16];
			data_memory[address+3] = dataW[31:24];
		end
		else if (access_size == `TWO_BYTES) begin
			data_memory[address] = dataW[7:0];
			data_memory[address+1] = dataW[15:8];
		end
		else if (access_size == `ONE_BYTE) begin
			data_memory[address] = dataW[7:0];
		end
			
	end
end
  

endmodule



