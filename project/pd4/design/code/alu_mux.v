module alu_mux(
  input alu_mux_sel,
  input [31:0] alu_mux_in1,
  input [31:0] alu_mux_in2,
  output reg [31:0] alu_mux_out
);

always @ (*) begin
	
	if(alu_mux_sel == 1'b0)
		alu_mux_out = alu_mux_in1;
	else
		alu_mux_out = alu_mux_in2;
end
 
  
 
endmodule


