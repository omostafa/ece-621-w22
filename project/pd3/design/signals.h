
/* Your Code Below! Enable the following define's 
 * and replace ??? with actual wires */
// ----- signals -----
// You will also need to define PC properly
`define F_PC                instr_addr
`define F_INSN              instr_out

`define D_PC                instr_addr
`define D_OPCODE            opcode
`define D_RD                addr_rd
`define D_RS1               addr_rs1
`define D_RS2               addr_rs2
`define D_FUNCT3            funct3
`define D_FUNCT7            funct7
`define D_IMM               igen_out
`define D_SHAMT             data_rs2

`define R_WRITE_ENABLE      write_enable
`define R_WRITE_DESTINATION addr_rd
`define R_WRITE_DATA        data_rd
`define R_READ_RS1          addr_rs1
`define R_READ_RS2          addr_rs2
`define R_READ_RS1_DATA     data_rs1
`define R_READ_RS2_DATA     data_rs2

`define E_PC                instr_addr
`define E_ALU_RES           alu_data_out
`define E_BR_TAKEN          b_taken

// ----- signals -----

// ----- design -----
`define TOP_MODULE                 pd3
// ----- design -----
