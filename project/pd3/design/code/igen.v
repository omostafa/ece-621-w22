`define I_TYPE		3'b000
`define S_TYPE		3'b001
`define B_TYPE		3'b010
`define U_TYPE		3'b011
`define J_TYPE		3'b100

module igen(
  input [2:0] igen_sel,
  input reg [31:0] igen_in,
  output reg [31:0] igen_out
);



always @ (*) begin
	
	if(igen_sel == `I_TYPE)
		igen_out = { {21{igen_in[31]}}, igen_in[30:25], igen_in[24:20] };
	else if(igen_sel == `S_TYPE)
		igen_out = { {21{igen_in[31]}}, igen_in[30:25], igen_in[11:7] };
	else if(igen_sel == `B_TYPE)
		igen_out = { {21{igen_in[31]}}, igen_in[30:25], igen_in[11:7] } << 1;
	else if(igen_sel == `J_TYPE) begin
		if(igen_in[31] == 1'b1)
			igen_out = (~{ {13{igen_in[31]}}, igen_in[19:12], igen_in[20], igen_in[30:21] }) + 1;
		else	
			igen_out = { {13{igen_in[31]}}, igen_in[19:12], igen_in[20], igen_in[30:21] };
	end
	else if(igen_sel == `U_TYPE) begin
		if(igen_in[31] == 1'b1)
			igen_out = (~ {igen_in[31:12], 12'h000}) + 1;
		else	
			igen_out = {igen_in[31:12], 12'h000};
	end
	else
		igen_out = 32'b0;
end
 
  
 
endmodule


