module dmemory(
  input clock,
  input reset,
  input reg [31:0] address,
  input reg [31:0] dataW,
  output reg [31:0] dataR,
  input reg read_write
);

  reg [7:0] data_memory [0:`MEM_DEPTH];
  
always @ (address) begin
	dataR = {data_memory[address+3], data_memory[address+2],
		 data_memory[address+1], data_memory[address]};
end 
 
always @ (posedge clock) begin
	if (read_write) begin
		data_memory[address] = dataW[7:0];
		data_memory[address+1] = dataW[15:8];
		data_memory[address+2] = dataW[23:16];
		data_memory[address+3] = dataW[31:24];
	end
end
  

endmodule



