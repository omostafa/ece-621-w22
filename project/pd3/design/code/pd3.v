// ALU operations

`define ADD 	6'b00_00_00
`define SUB 	6'b00_00_01
`define AND 	6'b00_00_10
`define OR	6'b00_00_11
`define XOR	6'b00_01_00
`define SLT	6'b00_01_01	// Less than signed
`define SLTU	6'b00_01_10	// Less than unsigned
`define SLL	6'b00_01_11	// Logic left shift
`define SRL	6'b00_10_00	// Logic right shift
`define SRA	6'b00_10_00	// Arithmetic right shift

// ALU operations

// Immediate types

`define I_TYPE		3'b000
`define S_TYPE		3'b001
`define B_TYPE		3'b010
`define U_TYPE		3'b011
`define J_TYPE		3'b100

// Immediate types

// R-type instructions

`define R_OPCODE 	7'b0110_011

`define ADD_ID  	10'b000_0000_000
`define SUB_ID  	10'b010_0000_000
`define SLL_ID  	10'b000_0000_001
`define SLT_ID		10'b000_0000_010
`define SLTU_ID	10'b000_0000_011
`define XOR_ID		10'b000_0000_100
`define SRL_ID		10'b000_0000_101
`define SRA_ID		10'b010_0000_101
`define OR_ID		10'b000_0000_110
`define AND_ID		10'b000_0000_111

// R-type instructions

// Immediate instructions

`define I_OPCODE	7'b0010_011

`define ADDI_ID	3'b000
`define SLTI_ID	3'b010
`define SLTIU_ID	3'b011
`define XORI_ID	3'b100
`define ORI_ID		3'b110
`define ANDI_ID	3'b111
`define SLLI_ID	3'b001
`define SRLI_SRAI_ID	3'b101

`define SRAI_UPPER	7'b010_0000
`define SRLI_UPPER	7'b000_0000


// Immediate instructions

// Load instructions

`define L_OPCODE	7'b000_0011

`define LB_ID		3'b000
`define LH_ID		3'b001
`define LW_ID		3'b010
`define LBU_ID		3'b100
`define LHU_ID		3'b101

// Load instructions

// Store instructions

`define S_OPCODE	7'b0100011

`define SB_ID		3'b000
`define SH_ID		3'b001
`define SW_ID		3'b010

// Store instructions

// Branch instructions

`define B_OPCODE	7'b1100011

`define BEQ_ID		3'b000
`define BNE_ID		3'b001
`define BLT_ID		3'b100
`define BGE_ID		3'b101
`define BLTU_ID	3'b110
`define BGEU_ID	3'b111

// Branch instructions

// Last five instructions

`define JALR_OPCODE	7'b1100111
`define JAL_OPCODE	7'b1101111
`define AUIPC_OPCODE	7'b0010111
`define LUI_OPCODE	7'b0110111
`define ECALL_OPCODE	7'b1110011

// Last five instructions

// Word-Half-Byte selects

`define WORD	3'b000
`define HALF	3'b001
`define BYTE	3'b010
`define HALFU	3'b011
`define BYTEU	3'b100

// Word-Half-Byte selects

module pd3(
  input clock,
  input reset
);

  // instruction memory module

  reg [31:0] instr_addr;
  reg [31:0] instr_in;
  reg [31:0] instr_out; // contains the instruction
  wire instr_wr_en;
  
  imemory imemory1( .clock(clock), .reset(reset), .instr_addr(instr_addr),
		     .instr_in(instr_in), .instr_out(instr_out), 
		     .instr_wr_en(instr_wr_en) );
		    
  		    
  // data memory module
  
  reg [31:0] data_addr;
  reg [31:0] dataW;
  reg [31:0] dataR;
  reg data_wr_en;
  
  dmemory dmemory1 ( .clock(clock), .reset(reset), .address(data_addr), .dataW(dataW),
  		      .dataR(dataR), .read_write(data_wr_en) );
  
  // register file
  
  reg [4:0] addr_rs1;
  reg [4:0] addr_rs2;
  reg [4:0] addr_rd;
  reg [31:0] data_rs1;
  reg [31:0] data_rs2;
  reg [31:0] data_rd;
  reg reg_wr_en;
  
  register_file register_file1( .clock(clock), .write_enable(reg_wr_en),
  			 .addr_rs1(addr_rs1), .addr_rs2(addr_rs2), .addr_rd(addr_rd),
  			 .data_rs1(data_rs1), .data_rs2(data_rs2), .data_rd(data_rd) );
  			 
  // ALU unit
  
  reg [5:0] alu_sel;
  reg [31:0] alu_data_in1;
  reg [31:0] alu_data_in2;
  reg [31:0] alu_data_out;
  
  alu alu1( .clock(clock), .reset(reset), .alu_sel(alu_sel), .alu_data_in1(alu_data_in1),
  	    .alu_data_in2(alu_data_in2), .alu_data_out(alu_data_out) );
  	    
  // ALU mux 1
  
  reg alu_mux1_sel;
  reg [31:0] alu_mux1_in1;
  reg [31:0] alu_mux1_in2;
  reg [31:0] alu_mux1_out;
  
  alu_mux alu_mux1( .alu_mux_sel(alu_mux1_sel), .alu_mux_in1(alu_mux1_in1),
  		     .alu_mux_in2(alu_mux1_in2), .alu_mux_out(alu_mux1_out) );
  		     
  // ALU mux 2
  
  reg alu_mux2_sel;
  reg [31:0] alu_mux2_in1;
  reg [31:0] alu_mux2_in2;
  reg [31:0] alu_mux2_out;
  
  alu_mux alu_mux2( .alu_mux_sel(alu_mux2_sel), .alu_mux_in1(alu_mux2_in1),
  		     .alu_mux_in2(alu_mux2_in2), .alu_mux_out(alu_mux2_out) );
  		     
  // ALU-PC mux
  
  reg alu_pc_mux_sel;
  reg [31:0] alu_pc_mux_in1;
  reg [31:0] alu_pc_mux_in2;
  reg [31:0] alu_pc_mux_out;
  
  alu_mux alu_pc_mux( .alu_mux_sel(alu_pc_mux_sel), .alu_mux_in1(alu_pc_mux_in1),
  		       .alu_mux_in2(alu_pc_mux_in2), .alu_mux_out(alu_pc_mux_out) );
  
  // Immediate unit
  
  reg [2:0] igen_sel;
  reg [31:0] igen_in;
  reg [31:0] igen_out;
  
  igen igen1(.igen_sel(igen_sel), .igen_in(igen_in), .igen_out(igen_out) );
  
  // Write back mux
  
  reg [1:0] wb_mux_sel;
  reg [31:0] wb_mux_in1;
  reg [31:0] wb_mux_in2;
  reg [31:0] wb_mux_in3;
  reg [31:0] wb_mux_out;
  
  wb_mux wb_mux1( .wb_mux_sel(wb_mux_sel), .wb_mux_in1(wb_mux_in1), .wb_mux_in2(wb_mux_in2),
  		   .wb_mux_in3(wb_mux_in3), .wb_mux_out(wb_mux_out) );
  
  // Word-Half-Byte select
  
  reg [2:0] whb_sel;
  reg [31:0] whb_sel_in;
  reg [31:0] whb_sel_out;
  
  whb_sel whb_sel1(.whb_sel(whb_sel), .whb_sel_in(whb_sel_in), .whb_sel_out(whb_sel_out) );
  
  // Branch-compare unit
  
  reg b_signed;
  reg [31:0] b_data_in1;
  reg [31:0] b_data_in2;
  reg b_eq;
  reg b_lt;
  
  b_comp b_comp1(.b_signed(b_signed), .b_data_in1(b_data_in1), .b_data_in2(b_data_in2),
  		  .b_eq(b_eq), .b_lt(b_lt) );
  		  
  // AND unit for alu_ouput <---> immediate
  
  reg [31:0] and_data_in1;
  reg [31:0] and_data_in2;
  reg [31:0] and_data_out;
  
  and_gate and_gate1(.and_data_in1(and_data_in1), .and_data_in2(and_data_in2),
  		      .and_data_out(and_data_out) );
  
  // Continuous assignments
  
  assign instr_wr_en = 0; // read-only for instructions
  
  always @ (*) begin
 
  	addr_rs1 = instr_out[19:15];
  	addr_rs2 = instr_out[24:20];
  	addr_rd = instr_out[11:7];
  	alu_data_in1 = alu_mux2_out;
  	alu_data_in2 = alu_mux1_out;
  	alu_mux1_in1 = data_rs2;
  	alu_mux1_in2 = igen_out;
  	alu_mux2_in1 = data_rs1;
  	alu_mux2_in2 = instr_addr;
  	alu_pc_mux_in1 = instr_addr + 4;
  	alu_pc_mux_in2 = and_data_out;
  	igen_in = instr_out;
  	data_rd = wb_mux_out;
  	data_addr = and_data_out;
  	wb_mux_in1 = whb_sel_out;
  	wb_mux_in2 = and_data_out;
  	wb_mux_in3 = instr_addr + 4;
  	and_data_in1 = alu_data_out;
  	whb_sel_in = dataR;
  	dataW = data_rs2;
  	b_data_in1 = data_rs1;
  	b_data_in2 = data_rs2;
  end
  
  // Incrementing PC or branching
  
    always @ (posedge clock) begin // This is ROM so no need to write to it
  	if(!reset) begin
  		instr_addr = alu_pc_mux_out;
  	end
  	else begin
  		instr_addr = 32'h01000000;
  	end
    end
  			 
  // Decoding logic
  
  reg [6:0] opcode;
  reg [2:0] funct3;
  reg [6:0] funct7;
  reg [9:0] R_ID;
  reg [2:0] I_ID;
  reg [2:0] L_ID;
  reg [2:0] S_ID;
  reg [2:0] B_ID;
  reg b_taken;
  
  always @ (*) begin
  
  	opcode = instr_out[6:0];
  	funct3 = instr_out[14:12];
  	funct7 = instr_out[31:25];
  	
  end
  
  always @ (posedge clock) begin
  	case (opcode)
  		`R_OPCODE: begin
  			R_ID = {funct7, funct3};
  			b_taken = 1'b0;
  			if(R_ID == `ADD_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `SUB_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `SUB;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `SLL_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `SLL;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `SLT_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `SLT;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `SLTU_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `SLTU;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `XOR_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `XOR;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `SRL_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `SRL;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `SRA_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `SRA;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `OR_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `OR;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(R_ID == `AND_ID) begin
  				wb_mux_sel = 2'b01;
  				alu_sel = `AND;
  				alu_mux1_sel = 1'b0;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  		end
  		`I_OPCODE: begin
  			I_ID = instr_out[14:12];
  			b_taken = 1'b0;
  			if(I_ID == `ADDI_ID) begin
  				wb_mux_sel = 2'b01;
  				igen_sel = `I_TYPE;
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  				$display(data_rd);
  			end
  			else if(I_ID == `SLTI_ID) begin
  				wb_mux_sel = 2'b01;
  				igen_sel = `I_TYPE;
  				alu_sel = `SLT;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(I_ID == `SLTIU_ID) begin
  				wb_mux_sel = 2'b01;
  				igen_sel = `I_TYPE;
  				alu_sel = `SLTU;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(I_ID == `XORI_ID) begin
  				wb_mux_sel = 2'b01;
  				igen_sel = `I_TYPE;
  				alu_sel = `XOR;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(I_ID == `ORI_ID) begin
  				wb_mux_sel = 2'b01;
  				igen_sel = `I_TYPE;
  				alu_sel = `OR;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(I_ID == `ANDI_ID) begin
  				wb_mux_sel = 2'b01;
  				igen_sel = `I_TYPE;
  				alu_sel = `AND;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(I_ID == `SLLI_ID) begin
  				wb_mux_sel = 2'b01;
  				igen_sel = `I_TYPE;
  				alu_sel = `SLL;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(I_ID == `SRLI_SRAI_ID) begin
  				if(instr_out[31:25] == `SRLI_UPPER) begin
  					wb_mux_sel = 2'b01;
  					igen_sel = `I_TYPE;
  					alu_sel = `SRL;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b0;
  					alu_pc_mux_sel = 1'b0;
  					reg_wr_en = 1'b1;
  					data_wr_en = 1'b0; // No data written to dmemory
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  				else if(instr_out[31:25] == `SRAI_UPPER) begin
  					wb_mux_sel = 2'b01;
  					igen_sel = `I_TYPE;
  					alu_sel = `SRA;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b0;
  					alu_pc_mux_sel = 1'b0;
  					reg_wr_en = 1'b1;
  					data_wr_en = 1'b0; // No data written to dmemory
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  			end
  		end
  		`L_OPCODE: begin
  			L_ID = instr_out[14:12];
  			b_taken = 1'b0;
  			if(L_ID == `LW_ID) begin
  				whb_sel = `WORD;
  				wb_mux_sel = 2'b00;
  				igen_sel = `I_TYPE;
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(L_ID == `LH_ID) begin
  				whb_sel = `HALF;
  				wb_mux_sel = 2'b00;
  				igen_sel = `I_TYPE;
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(L_ID == `LB_ID) begin
  				whb_sel = `BYTE;
  				wb_mux_sel = 2'b00;
  				igen_sel = `I_TYPE;
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(L_ID == `LHU_ID) begin
  				whb_sel = `HALFU;
  				wb_mux_sel = 2'b00;
  				igen_sel = `I_TYPE;
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(L_ID == `LBU_ID) begin
  				whb_sel = `BYTEU;
  				wb_mux_sel = 2'b00;
  				igen_sel = `I_TYPE;
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b1;
  				data_wr_en = 1'b0; // No data written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  		end
  		`S_OPCODE: begin
  			S_ID = instr_out [14:12];
  			b_taken = 1'b0;
  			if(S_ID == `SB_ID) begin
  				whb_sel = `BYTE;
  				wb_mux_sel = 2'b00; // don't care
  				igen_sel = `S_TYPE; // S type immediate
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b0; // nothing is written to registers
  				data_wr_en = 1'b1; // Data IS written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(S_ID == `SH_ID) begin
  				whb_sel = `HALF;
  				wb_mux_sel = 2'b00; // don't care
  				igen_sel = `S_TYPE; // S type immediate
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b0; // nothing is written to registers
  				data_wr_en = 1'b1; // Data IS written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  			else if(S_ID == `SW_ID) begin
  				whb_sel = `WORD;
  				wb_mux_sel = 2'b00; // don't care
  				igen_sel = `S_TYPE; // S type immediate
  				alu_sel = `ADD;
  				alu_mux1_sel = 1'b1;
  				alu_mux2_sel = 1'b0;
  				alu_pc_mux_sel = 1'b0;
  				reg_wr_en = 1'b0; // nothing is written to registers
  				data_wr_en = 1'b1; // Data IS written to dmemory
  				and_data_in2 = 32'hFF_FF_FF_FF;
  			end
  		end
  		`B_OPCODE: begin
  			B_ID = instr_out[14:12];
  			if(B_ID == `BEQ_ID) begin
  				b_signed = 1'b1;
  				if(b_eq) begin
  					b_taken = 1'b1;
  					whb_sel = `WORD;
  					wb_mux_sel = 2'b00; // don't care
  					igen_sel = `B_TYPE;
  					alu_sel = `ADD;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b1;
  					alu_pc_mux_sel = 1'b1;
  					reg_wr_en = 1'b0; // nothing is written to registers
  					data_wr_en = 1'b0;
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  				else
  					b_taken = 1'b0;
  			end
  			else if(B_ID == `BNE_ID) begin
  				b_signed = 1'b1;
  				if(!b_eq) begin
  					b_taken = 1'b1;
  					whb_sel = `WORD;
  					wb_mux_sel = 2'b00; // don't care
  					igen_sel = `B_TYPE;
  					alu_sel = `ADD;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b1;
  					alu_pc_mux_sel = 1'b1;
  					reg_wr_en = 1'b0; // nothing is written to registers
  					data_wr_en = 1'b0;
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  				else
  					b_taken = 1'b0;
  			end
  			else if(B_ID == `BLT_ID) begin
  				b_signed = 1'b1;
  				if(b_lt) begin
  					b_taken = 1'b1;
  					whb_sel = `WORD;
  					wb_mux_sel = 2'b00; // don't care
  					igen_sel = `B_TYPE;
  					alu_sel = `ADD;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b1;
  					alu_pc_mux_sel = 1'b1;
  					reg_wr_en = 1'b0; // nothing is written to registers
  					data_wr_en = 1'b0;
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  				else
  					b_taken = 1'b0;
  			end
  			else if(B_ID == `BGE_ID) begin
  				b_signed = 1'b1;
  				if(!b_lt) begin
  					b_taken = 1'b1;
  					whb_sel = `WORD;
  					wb_mux_sel = 2'b00; // don't care
  					igen_sel = `B_TYPE;
  					alu_sel = `ADD;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b1;
  					alu_pc_mux_sel = 1'b1;
  					reg_wr_en = 1'b0; // nothing is written to registers
  					data_wr_en = 1'b0;
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  				else
  					b_taken = 1'b0;
  			end
  			else if(B_ID == `BLTU_ID) begin
  				b_signed = 1'b0;
  				if(b_lt) begin
  					b_taken = 1'b1;
  					whb_sel = `WORD;
  					wb_mux_sel = 2'b00; // don't care
  					igen_sel = `B_TYPE;
  					alu_sel = `ADD;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b1;
  					alu_pc_mux_sel = 1'b1;
  					reg_wr_en = 1'b0; // nothing is written to registers
  					data_wr_en = 1'b0;
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  				else
  					b_taken = 1'b0;
  			end
  			else if(B_ID == `BGEU_ID) begin
  				b_signed = 1'b0;
  				if(!b_lt) begin
  					b_taken = 1'b1;
  					whb_sel = `WORD;
  					wb_mux_sel = 2'b00; // don't care
  					igen_sel = `B_TYPE;
  					alu_sel = `ADD;
  					alu_mux1_sel = 1'b1;
  					alu_mux2_sel = 1'b1;
  					alu_pc_mux_sel = 1'b1;
  					reg_wr_en = 1'b0; // nothing is written to registers
  					data_wr_en = 1'b0;
  					and_data_in2 = 32'hFF_FF_FF_FF;
  				end
  				else
  					b_taken = 1'b0;
  			end
  		end
  		`JALR_OPCODE: begin
  			b_taken = 1'b0;
  			whb_sel = `WORD;
  			wb_mux_sel = 2'b10; // Write PC+4 to rd
  			igen_sel = `I_TYPE;
  			alu_sel = `ADD;
  			alu_mux1_sel = 1'b1;
  			alu_mux2_sel = 1'b0;
  			alu_pc_mux_sel = 1'b1;
  			reg_wr_en = 1'b1;
  			data_wr_en = 1'b0;
  			and_data_in2 = 32'hFF_FF_FF_FE;
  		end
  		`JAL_OPCODE: begin // Jump relative to PC
  			b_taken = 1'b0;
  			whb_sel = `WORD;
  			wb_mux_sel = 2'b10; // Write PC+4 to rd
  			igen_sel = `J_TYPE;
  			if(instr_out[31] == 1'b1)
  				alu_sel = `SUB;
  			else
  				alu_sel = `ADD;
  			alu_mux1_sel = 1'b1;
  			alu_mux2_sel = 1'b1;
  			alu_pc_mux_sel = 1'b1;
  			reg_wr_en = 1'b1;
  			data_wr_en = 1'b0;
  			and_data_in2 = 32'hFF_FF_FF_FE;
  		end
  		`AUIPC_OPCODE: begin // Jump relative to PC
  			b_taken = 1'b0;
  			whb_sel = `WORD;
  			wb_mux_sel = 2'b01; // Write alu-ouput to rd
  			igen_sel = `U_TYPE;
  			if(instr_out[31] == 1'b1)
  				alu_sel = `SUB;
  			else
  				alu_sel = `ADD;
  			alu_mux1_sel = 1'b1;
  			alu_mux2_sel = 1'b1;
  			alu_pc_mux_sel = 1'b1; // Write alu-output to pc
  			reg_wr_en = 1'b1;
  			data_wr_en = 1'b0;
  			and_data_in2 = 32'hFF_FF_FF_FF;
  		end
  		`LUI_OPCODE: begin // Jump relative to PC
  			b_taken = 1'b0;
  			whb_sel = `WORD;
  			wb_mux_sel = 2'b01; // Write alu-ouput to rd
  			igen_sel = `U_TYPE;
  			if(instr_out[31] == 1'b1)
  				alu_sel = `SUB;
  			else
  				alu_sel = `ADD;
  			alu_mux1_sel = 1'b1;
  			alu_mux2_sel = 1'b1;
  			alu_pc_mux_sel = 1'b0;
  			reg_wr_en = 1'b1;
  			data_wr_en = 1'b0;
  			and_data_in2 = 32'hFF_FF_FF_FF;
  		end
  		`ECALL_OPCODE: begin
  			b_taken = 1'b0;
  		end
  		default: 
  			;
  	endcase
  end
 
  		    
  




endmodule
