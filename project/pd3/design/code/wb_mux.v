module wb_mux(
  input [1:0] wb_mux_sel,
  input reg [31:0] wb_mux_in1,
  input reg [31:0] wb_mux_in2,
  input reg [31:0] wb_mux_in3,
  output reg [31:0] wb_mux_out
);

always @ (*) begin
	
	if(wb_mux_sel == 2'b00)
		wb_mux_out = wb_mux_in1;
	else if(wb_mux_sel == 2'b01)
		wb_mux_out = wb_mux_in2;
	else
		wb_mux_out = wb_mux_in3;
end
 
  
 
endmodule


