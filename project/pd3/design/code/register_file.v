module register_file(
  input clock,
  input write_enable,
  input reg [4:0] addr_rs1,
  input reg [4:0] addr_rs2,
  input reg [4:0] addr_rd, // address to write to
  output reg [31:0] data_rs1, // corresponds to rs1
  output reg [31:0] data_rs2, // corresponds to rs2
  input reg [31:0] data_rd // data to write into a register
);

reg [31:0] registers [0:31];

always @ (addr_rs1) begin
	data_rs1 = registers[addr_rs1];
end

always @ (addr_rs2) begin
	data_rs2 = registers[addr_rs2];
end

always @ (posedge clock) begin
	if(write_enable)
		registers[addr_rd] = data_rd;
end
 
  
 
endmodule


