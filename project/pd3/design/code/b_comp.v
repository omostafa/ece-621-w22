module b_comp(
  input reg b_signed,
  input reg [31:0] b_data_in1,
  input reg [31:0] b_data_in2,
  output reg b_eq,
  output reg b_lt
);


always @ (*) begin
	
	if(b_signed) begin
		if (b_data_in1[31] == 1'b0 && b_data_in2[31] == 1'b0) begin
			b_lt = b_data_in1 < b_data_in2;
			b_eq = !b_lt;
		end
		else if (b_data_in1[31] == 1'b1 && b_data_in2[31] == 1'b0) begin
			b_lt = 1'b1;
			b_eq = !b_lt;
		end
		else if (b_data_in1[31] == 1'b0 && b_data_in2[31] == 1'b1) begin
			b_lt = 1'b0;
			b_eq = !b_lt;
		end
		else begin	
			b_lt = ((~b_data_in1) + 1'b1) > ((~b_data_in2) + 1'b1);
			b_eq = !b_lt;	 
		end
	end
	else begin
		
		b_lt = b_data_in1 < b_data_in2;
		b_eq = !b_lt;	
		
	end
		
end
 
  
 
endmodule


