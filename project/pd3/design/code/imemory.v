module imemory(
  input clock,
  input reset,
  input reg [31:0] instr_addr,
  input reg [31:0] instr_in,
  output reg [31:0] instr_out,
  input reg instr_wr_en
);
  
  reg [31:0] pc;
  reg [32:0] temp_memory [0:`MEM_DEPTH];
  reg [7:0]  instruction_memory [0:`MEM_DEPTH];
  integer i;

  initial begin
	$readmemh(`MEM_PATH, temp_memory);
	pc = 32'h01000000;
	for(i=0; i < `LINE_COUNT; i = i + 1)
		begin
			instruction_memory[pc] = temp_memory[i][7:0];
			instruction_memory[pc+1] = temp_memory[i][15:8];
			instruction_memory[pc+2] = temp_memory[i][23:16];
			instruction_memory[pc+3] = temp_memory[i][31:24];
			pc = pc + 4;
		end
  end
  
  
always @ (instr_addr) begin
	instr_out = {instruction_memory[instr_addr+3], 
			   instruction_memory[instr_addr+2],
		    	   instruction_memory[instr_addr+1], 
		    	   instruction_memory[instr_addr]};
end
  
always @ (posedge clock) begin
	if (instr_wr_en) begin
		instruction_memory[instr_addr] = instr_in[7:0];
		instruction_memory[instr_addr+1] = instr_in[15:8];
		instruction_memory[instr_addr+2] = instr_in[23:16];
		instruction_memory[instr_addr+3] = instr_in[31:24];
	end
end
  
  

endmodule



