module pd1(
  input clock,
  input reset
);

  reg [31:0] address;
  reg [31:0] data_in;
  reg [31:0] data_out;
  reg read_write;
  
  imemory imemory1( .clock(clock), .reset(reset), .address(address),
  		    .data_in(data_in), .data_out(data_out), .read_write(read_write));
  
  

endmodule



