module imemory(
  input clock,
  input reset,
  input reg [31:0] address,
  input reg [31:0] data_in,
  output reg [31:0] data_out,
  input reg read_write
);
  
  reg [32:0] temp_memory [0:`MEM_DEPTH];
  reg [7:0] main_memory [0:`MEM_DEPTH];
  integer i;

  initial begin
	$readmemh(`MEM_PATH, temp_memory);
	address = 32'h01000000;
	for(i=0; i < `LINE_COUNT; i = i + 1)
		begin
			main_memory[address] = temp_memory[i][7:0];
			main_memory[address+1] = temp_memory[i][15:8];
			main_memory[address+2] = temp_memory[i][23:16];
			main_memory[address+3] = temp_memory[i][31:24];
			address = address + 4;
		end
	address = 32'h01000000;
  end
  
  always @ (posedge clock) begin
  	if(!reset) begin
  		address = address + 4;
  	end
  	else begin
  		address = 32'h01000000;
  	end
  end
  
always @ (address) begin
	data_out = {main_memory[address+3], main_memory[address+2], main_memory[address+1], main_memory[address]};
end
  
always @ (posedge clock) begin
	if (read_write) begin
		main_memory[address] = data_in[7:0];
		main_memory[address+1] = data_in[15:8];
		main_memory[address+2] = data_in[23:16];
		main_memory[address+3] = data_in[31:24];
	end
end
  
  

endmodule



